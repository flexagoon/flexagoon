![a cool header, too bad you can't see it](https://gitlab.com/flexagoon/flexagoon/-/raw/main/assets/smart-header.png)

# :wave: Hey, welcome to my GitLab page!

Make sure to check out [my GitHub](https://github.com/flexagoon) as well!

## :information_source: Click on any of the badges to learn more

## :computer: System info

[![OS: NixOS](https://shields.io/badge/OS-NixOS-5277C3?style=for-the-badge&logoColor=white&logo=NixOS)](https://nixos.org)
[![Editor: Vscodium](https://shields.io/badge/Editor-Vscodium-007ACC?style=for-the-badge&logoColor=white&logo=Visual%20Studio%20Code)](https://vscodium.com)
[![DE: KDE Plasma](https://shields.io/badge/DE-KDE%20Plasma-1D99F3?style=for-the-badge&logoColor=white&logo=KDE)](https://kde.org)
[![Browser: Firefox](https://shields.io/badge/Browser-Firefox-FF7139?style=for-the-badge&logoColor=white&logo=Firefox%20Browser)](https://mozilla.org/firefox)

[![Laptop: ASUS ROG Strix GL702VS](https://shields.io/badge/%F0%9F%92%BB%20Laptop-ASUS%20ROG%20Strix%20GL702VS-ED1C24?style=for-the-badge&logoColor=white)](https://www.dictionary.com/browse/shit)

## :sunglasses: Skills

### :speech_balloon: Languages

[![Dart](https://shields.io/badge/Dart-0175C2?style=for-the-badge&logoColor=white&logo=dart)](https://dart.dev)
[![Python](https://shields.io/badge/Python-3776AB?style=for-the-badge&logoColor=white&logo=python)](https://python.org)
[![C#](<https://shields.io/badge/Microsoft%20Java%20(C%23)-239120?style=for-the-badge&logoColor=white&logo=c%20sharp>)](https://docs.microsoft.com/en-us/dotnet/csharp)
[![Bash](https://shields.io/badge/Bash-4EAA25?style=for-the-badge&logoColor=white&logo=gnu+bash)](https://www.gnu.org/software/bash)
[![Nix](https://shields.io/badge/Nix-5277C3?style=for-the-badge&logoColor=white&logo=nixos)](https://nixos.org)

### :key: SDKs

[![Flutter](https://shields.io/badge/Flutter-02569B?style=for-the-badge&logoColor=white&logo=flutter)](https://flutter.dev)
[![SpigotMC](https://shields.io/badge/SpigotMC-FF6A00?style=for-the-badge&logoColor=white&logo=mojang+studios)](https://spigotmc.org)

### :globe_with_meridians: Other

[![Telegram API](https://shields.io/badge/Telegram%20API-26A5E4?style=for-the-badge&logoColor=white&logo=telegram)](https://core.telegram.org)
[![LaTeX](https://shields.io/badge/LaTeX-008080?style=for-the-badge&logoColor=white&logo=latex)](https://latex-project.org)
[![OSM Mapping](https://shields.io/badge/OSM%20Mapping-7EBC6F?style=for-the-badge&logoColor=white&logo=openstreetmap)](https://osm.org)
[![Git](https://shields.io/badge/Git%20(obviously%20lol)-F05032?style=for-the-badge&logoColor=white&logo=git)](https://git-scm.com)

## :mailbox: Social Media

[![Telegram](https://shields.io/badge/Telegram--0?style=social&logo=telegram)](https://t.me/flexagoon)
[![Reddit](https://shields.io/badge/Reddit--0?style=social&logo=reddit)](https://reddit.com/u/sevenisnotanumber2)
[![OSM](https://shields.io/badge/OSM--0?style=social&logo=openstreetmap&logoColor=7EBC6F)](https://osm.org/user/flexagoon)
[![Mastodon](https://shields.io/badge/Mastodon--0?style=social&logo=mastodon)](https://koyu.space/@flexagoon)
[![Email](https://shields.io/badge/Email--0?style=social&logo=disroot)](mailto:contact@flexagoon.com)
